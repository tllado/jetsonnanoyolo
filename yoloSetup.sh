#!/bin/bash

# This program installs YOLO vs and configures it to run on the Nvidia Jetson 
# Nano

echo "Installing YOLO"

git clone https://github.com/pjreddie/darknet.git
cd darknet

# Modify Makefile for Jetson Nano
sed -i 's/GPU=0/GPU=1/' Makefile
sed -i 's/CUDNN=0/CUDNN=1/' Makefile
sed -i 's/OPENCV=0/OPENCV=1/' Makefile

# Modify config file for Jetson Nano
sed -i 's/batch=64/batch=1/' cfg/yolov3.cfg
sed -i 's/subdivisions=16/subdivisions=1/' cfg/yolov3.cfg
sed -i 's/width=608/width=220/' cfg/yolov3.cfg
sed -i 's/height=608/height=220/' cfg/yolov3.cfg

# Give access to CUDA
export LD_LIBRARY_PATH=/usr/local/cuda/bin
export PATH=$PATH:/usr/local/cuda/bin
make

# Download YOLO weights
wget https://pjreddie.com/media/files/yolov3.weights
wget https://pjreddie.com/media/files/yolov3-tiny.weights

echo "Setup Complete"
