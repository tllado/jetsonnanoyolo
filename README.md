# jetsonNanoYolo

A few scripts to configure an Nvidia Jetson Nano to run YOLO v3

0. Connect camera with system de-energized
1. Enable automatic fan control with `./fanSetup.sh`
2. Build YOLO v3 with `./yoloSetup.sh`
3. Run YOLO v3 with `./yoloRun.sh USB` for USB camera or `./yoloRun.sh RPI` for RasPi camera

