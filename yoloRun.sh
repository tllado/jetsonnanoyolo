#!/bin/bash

# This script executes YOLO v3 on an Nvidia Jetson Nano board. It assumes you've
# already installed Yolo v3 and configured it to run on the Jetson Nano.

runUSB () {
    echo 'Executing with USB Camera ...'
    ./darknet detector demo cfg/coco.data cfg/yolov3.cfg yolov3.weights
}

runRPI () {
    echo 'Executing with CSi Camera ...'
    # taken from https://github.com/alsolh/jetson-nano
    ./darknet detector demo cfg/coco.data cfg/yolov3.cfg yolov3.weights "nvarguscamerasrc auto-exposure=1 ! video/x-raw(memory:NVMM), width=(int)1280, height=(int)720, format=(string)NV12, framerate=(fraction)60/1 ! nvvidconv flip-method=0 ! video/x-raw, width=(int)1280, height=(int)720, format=(string)BGRx ! videoconvert ! video/x-raw, format=(string)BGR ! appsink -e"
}

cd darknet

# If we didn't receive an argument, default to USB
if [ -z ${1+x} ]
then
    runUSB
# If we did receive an argument, use that
else
    if [ ${1^^} = 'USB' ]
    then
        runUSB
    elif [ ${1^^} = 'CSI' ] || [ ${1^^} = 'RPI' ]
    then
        runRPI
    else
        echo "Invalid Input \"" $1 "\""
	echo "Options are \"USB\" and \"CSi\""
    fi
fi

