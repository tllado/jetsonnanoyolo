#!/bin/bash

# This script enables an included program that automatically controls fan speed
# It sets this program to execute at startup

# Activate automatic fan control at boot
(sudo crontab -l 2>/dev/null; echo "@reboot sudo /usr/bin/jetson_clocks") | sudo crontab -
echo "Fan Control Setup Complete"

# Go ahead and start fan now
sudo /usr/bin/jetson_clocks
